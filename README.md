# SKPhysicsBody Demo

This is a demo of how to use `SKPhysicsBody`.
Use `SKPhysicsBody` to add hitboxes, gravity, collisions, mass, and bounce to your sprites.

![](screenshots/physics.gif)


