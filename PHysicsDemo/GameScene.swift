//
//  GameScene.swift
//  PHysicsDemo
//
//  Created by Parrot on 2019-02-13.
//  Copyright © 2019 Parrot. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    let circle = SKSpriteNode(imageNamed: "circle")
    let square = SKSpriteNode(imageNamed: "square")
    let triangle = SKSpriteNode(imageNamed: "triangle")
    let lshape = SKSpriteNode(imageNamed:"L")
    
    var timeOfLastUpdate:TimeInterval = 0
    var dt: TimeInterval = 0
        
    override func didMove(to view: SKView) {
        // THE GAME SCENE
        // ---------------------
        // set the physics properties of this world
        self.backgroundColor = UIColor.white
        
        // set boundaries around the scene
        self.physicsBody = SKPhysicsBody(edgeLoopFrom: self.frame)

        
        // SPRITES
        // ---------------------
        circle.position = CGPoint(x:self.size.width*0.25, y:self.size.height/2)
        square.position = CGPoint(x:self.size.width/2, y:self.size.height/2)
        triangle.position = CGPoint(x:self.size.width*0.75, y:self.size.height/2)
        
        // add physics to circle
        circle.physicsBody = SKPhysicsBody(circleOfRadius: circle.size.width / 2)
        self.circle.physicsBody?.affectedByGravity = false
        
        // add physics to square
        self.square.physicsBody = SKPhysicsBody(rectangleOf:self.square.frame.size)
        
        // add physics to triangle
        self.triangle.physicsBody = SKPhysicsBody(rectangleOf:self.triangle.frame.size)
        
        self.triangle.physicsBody?.affectedByGravity = false
        
        // add L
        self.lshape.name = "shape"
        self.lshape.position = CGPoint(x: self.size.width * 0.5,
                             y: self.size.height * 0.75)
        self.lshape.physicsBody = SKPhysicsBody(texture: self.lshape.texture!, size: self.lshape.size)
        
        addChild(circle)
        addChild(square)
        addChild(triangle)
        addChild(lshape)
    }
    func spawnSand() {
        let sand = SKSpriteNode(imageNamed:"sand")
        
        // put sand at a random (x,y) position
        let x = self.size.width/2
        let y = self.size.height - 100
        sand.position.x = x
        sand.position.y = y
        
        // add physics
        sand.physicsBody = SKPhysicsBody(circleOfRadius: sand.size.width / 2)
        self.circle.physicsBody?.affectedByGravity = true
        
        // make sand bounce
        sand.physicsBody!.restitution = 1.0
        
        // make sand 20x heavier than normal
        sand.physicsBody!.density = 20.0
        
        addChild(sand)
    }

    
    override func update(_ currentTime: TimeInterval) {
        // make new sand every 10ms
        self.dt = currentTime - timeOfLastUpdate
        if (self.dt >= 0.1) {
            timeOfLastUpdate = currentTime
            self.spawnSand()
        }
    }
    

}
